<?php

namespace App\Services;

use App\Jobs\ParticipationApplicationAuctionLots;
use App\Models\Procedure\ParticipationApplication;
use App\Models\Procedure\ParticipationApplication\AutoBot;
use App\Models\Procedure;
use App\Models\Procedure\Auction;
use App\Services\AuthService;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use \Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class Participation pplicationService
 * @package App\Services
 */
class ParticipationApplicationService extends BaseService
{
    /**
     * @var Log model
     */
    protected ParticipationApplication $participationApplicationModel;

    /**
     * @var Log model
     */
    protected Procedure $procedureModel;

    /**
     * @var AuthService
     */
    protected AuthService $authService;

    /**
     * @var Auction model
     */
    protected Auction $auctionModel;

    /**
     * @var AutoBot model
     */
    protected AutoBot $autoBotModel;


    /**
     * SettingService constructor.
     */
    public function __construct()
    {
        $this->participationApplicationModel = new ParticipationApplication();
        $this->procedureModel = new Procedure();
        $this->authService = new AuthService();
        $this->auctionModel = new Auction();
        $this->autoBotModel = new AutoBot();
    }

    /**
     * @param int $ProcedureId
     * @param array $input
     * @return LengthAwarePaginator
     */
    public function index(array $input): LengthAwarePaginator
    {
        $userData = $this->authService->getCurrentUserData();
        $userCompanies = Arr::pluck($userData['companies'], 'inn');

        $query = $this->participationApplicationModel->newQuery();
        $query = $query->whereIn('inn',$userCompanies)
            ->when($input['filter']['inn'] ?? null, function (Builder $query, $inn) {
                return $query->whereIn('inn', $inn);
            })
            ->when($input['filter']['procedure_ids'] ?? null, function (Builder $query, $procedureIds) {
                return $query->whereIn('procedure_id', $procedureIds);
            })
            ->when($input['filter']['application_status'] ?? null, function (Builder $query, $status) {
                return $query->whereIn('status', $status);
            });

        if (empty($input['draft'])) {
            $query = $query->whereNotIn('status', ['draft']);
        } else {
            $query->where('status','=', 'draft');
        }

        $query->orderBy('updated_at', $input['filter']['order']['publication_date'] ?? 'desc');
        $query->with(['subject','autobot','procedure.purchaseSubject', 'procedure.purchaseTerm']);

        return $query->paginate();
    }

    public function procedureIndex(int $procedureId, $data): LengthAwarePaginator
    {
        $procedure = $this->procedureModel->findOrFail($procedureId);
        $this->access('listProcedureApplications',[$this->participationApplicationModel, $procedure]);

        $query = $this->participationApplicationModel->newQuery()
            ->when($data['lot_number'] ?? null, function (Builder $query, $lotNumber) {
                return $query->whereIn('id', function(\Illuminate\Database\Query\Builder $query) use ($lotNumber) {
                   return $query->select((new Procedure\ParticipationApplication\Subject())->getTable().'.procedure_participation_applications_id')
                        ->from((new Procedure\ParticipationApplication\Subject\Product())->getTable())
                        ->join((new Procedure\ParticipationApplication\Subject())->getTable(),(new Procedure\ParticipationApplication\Subject\Product())->getTable().'.participation_application_subjects_id','=', (new Procedure\ParticipationApplication\Subject())->getTable().'.id')
                    ->where((new Procedure\ParticipationApplication\Subject\Product())->getTable().'.lot','=',$lotNumber);
                })->where('status','!=','canceled');
            })
            ->when($data['filter']['rebidding']?? null, function (Builder $query, $rebidding) {
                $query->WhereHas('auction');
                $query->orWhereHas('subject', function (Builder $query) {
                    $query->whereNotNull('price_after_rebidding');
                });

                return $query;
            });
        $query->when($data['filter']['ids'] ?? null, function (Builder $query, $ids) {
            foreach ($ids AS $idPart) {
                $query->orWhere('id','LIKE','%'.$idPart.'%');
            }
            return $query;
        });
        $query->when($data['filter']['application_status'] ?? null, function (Builder $query, $statuses) {
            return $query->whereIn('procedure_participation_applications.status',$statuses);
        });
        $query->when($data['filter']['price_after_rebidding'] ?? null, function (Builder $query, $price_after_rebidding) {
            return $query->whereHas('subject', function (Builder $query) {
                $query->whereNotNull('price_after_rebidding');
            });
        });
        $query->where('procedure_participation_applications.procedure_id','=', $procedureId)
        ->whereNotIn('procedure_participation_applications.status',['draft','canceled']);
        $query->orderBy((new Procedure\ParticipationApplication())->getTable().'.updated_at', $data['order']['publication_date'] ?? 'desc');

        $query->with(['subject','autobot','procedure.purchaseSubject', 'procedure.purchaseTerm','procedure.rebiddingInfo']);

        return $query->paginate();
    }

    /**
     * @param $procedureId
     * @param $inn
     * @return mixed
     */
    public function ShowParticipationApplication($applicationId)
    {
        $item = ParticipationApplication::with([
            'subject.products.purchaseProduct',
            'autobot',
            'procedure.purchaseSubject.products',
            'procedure.purchaseTerm',
            'procedure.guarantee',
            'procedure.rebiddingInfo',
        ])
            ->where(['id' => $applicationId])
            ->whereNotIn('status',['canceled'])->orderByDesc('updated_at')->first();

        $this->access('showParticipationApplication',[$item]);

        return $item;
    }

    /**
     * @param int $procedureId
     * @param array $data
     * @return mixed
     */
    public function storeParticipationApplication(int $procedureId, array $data)
    {
        $procedure = $this->procedureModel->with(['purchaseSubject.products'])->findOrFail($procedureId);

        $this->access('storeApplication',[$this->participationApplicationModel,$procedure, $data['inn']]);

        $oldDraft = $this->participationApplicationModel->query()->where(['procedure_id' => $procedureId, 'inn' => $data['inn'], 'status' => 'draft'])->first();
        if ($oldDraft) {
            $mediaItems = $oldDraft->getMedia($this->participationApplicationModel->getApplicationMediaCollectionName());
        }
        $procedureProducts = [];
        foreach ($procedure->purchaseSubject->products AS $product) {
            $procedureProducts[$product->id] = $product->toArray();
        }

        $inputApplication = [
            'procedure_id' => $procedureId,
            'tender_trading_type' => $data['tender_trading_type'],
            'inn' => $data['inn'],
            'description' => !empty($data['description']) ? $data['description'] : null,
        ];
        $application = $this->participationApplicationModel->create($inputApplication);
        if ($oldDraft) {
            foreach ($mediaItems AS $item) {
                if (isset($data['documents_old_ids']) && in_array($item->id,$data['documents_old_ids'])) {
                    $item->move($application,$this->participationApplicationModel->getApplicationMediaCollectionName());
                }
            }
            $oldDraft->delete();
        }
        if (!empty($data['documents'])) {
            foreach ($data['documents'] as $group => $documentsArray) {
                foreach ($documentsArray AS $inputDocument) {
                    $application->addMedia($inputDocument)
                        ->withCustomProperties(['group' => $group])
                        ->toMediaCollection($this->participationApplicationModel->getApplicationMediaCollectionName());
                }
            }
        }

        $sumByLot = [];
        $sumByLotNoVat = [];

        $inputSubject = [
            'procedure_purchase_subjects_id' => $procedure->purchaseSubject->id
        ];
        $createdSubject = $application->subject()->create($inputSubject);
        foreach ($data['products'] as $product) {
            if (!isset($procedureProducts[$product['product_id']])) abort(422,'Missing!');

            $priceForOne = !empty($product['price_for_one']) ? $product['price_for_one'] : $procedureProducts[$product['product_id']]['price_for_one'];
            $vat = !empty($product['vat']) ? $product['vat'] : $procedureProducts[$product['product_id']]['vat'];

            $amountPerPosition = ($procedureProducts[$product['product_id']]['quantity'] * $priceForOne)
                * (1+($vat/100));
            $amountPerPositionNoVat = ($procedureProducts[$product['product_id']]['quantity'] * $priceForOne);

            $lotNumber = $procedureProducts[$product['product_id']]['lot'];

            $oldSumByLot = isset($sumByLot[$lotNumber]) ? $sumByLot[$lotNumber] : 0;
            $oldSumByLotNoVat = isset($sumByLotNoVat[$lotNumber]) ? $sumByLotNoVat[$lotNumber] : 0;

            $sumByLot[$lotNumber] = $oldSumByLot + $amountPerPosition;
            $sumByLotNoVat[$lotNumber] = $oldSumByLotNoVat + $amountPerPositionNoVat;

            $inputProduct = [
                'purchase_subject_products_id' => $product['product_id'],
                'name' => isset($product['name']) ? $product['name'] : null,
                'category_okpd2' => isset($product['category_okpd2']) ? $product['category_okpd2'] : null,
                'quantity' => isset($product['quantity']) ? $product['quantity'] : null,
                'measure' => isset($product['measure']) ? $product['measure'] : null,
                'currency' => isset($product['currency']) ? $product['currency'] : null,
                'marksize_id' => isset($product['marksize_id']) ? $product['marksize_id'] : null,
                'price_for_one' => isset($product['price_for_one']) ? $product['price_for_one'] : null,
                'vat' => isset($product['vat']) ? $product['vat'] : null,
                'amount_per_position' => $amountPerPosition,
                'country' => isset($product['country']) ? $product['country'] : null,

            ];
            $createdSubject->products()->create($inputProduct);
        }

        $lostsNumber = count($sumByLot); 
        ksort($sumByLot); 
        ksort($sumByLotNoVat); 
        $lotAmounts = $sumByLot;
        $startPrice = array_sum($sumByLot); 
        $startPriceNoVat = array_sum($sumByLotNoVat); 
        $createdSubject->update([
            'lots_number' => $lostsNumber,
            'lot_amounts' => $lotAmounts,
            'start_price' => $startPrice,
            'start_price_no_vat' => $startPriceNoVat
        ]);

        return ParticipationApplication::with([
            'subject.products.purchaseProduct',
            'procedure.purchaseSubject.products',
            'procedure.purchaseTerm',
            'procedure.guarantee',
            'procedure.rebiddingInfo',
        ])->find($application->id);
    }

    public function changeStatus($data, $applicationId)
    {
        $application = $this->participationApplicationModel->findOrFail($applicationId);
        $procedure = $this->procedureModel->with(['purchaseSubject.products'])->findOrFail($application->procedure_id);

        $this->access('changeStatus',[$application,$procedure,$data['status']]);

        $application->status = $data['status'];
        $application->save();

        return $data['status'];
    }

    /**
     * @param $data
     * @param $applicationId
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function patchProducts($data, $applicationId, $lot)
    {
        $application = $this->participationApplicationModel->with(['subject.products','procedure.rebiddingInfo'])->findOrFail($applicationId);
        $this->access('patchProducts',[$application,$lot]);

        $keyedProducts = collect($data->products)->keyBy('product_id')->all();
        $LotAmountRebidding = 0;
        DB::beginTransaction();
        foreach ($application->subject->products AS &$product) {
            if ( isset($keyedProducts[$product->purchase_subject_products_id]) && $product->lot == $lot ) {
                if ( !empty($product->price_for_one_rebidding) ) {
                    DB::rollBack();
                    abort(422,'Application allready sent!!');
                }
                if ( $keyedProducts[$product->purchase_subject_products_id]['price_for_one_rebidding'] > $product->price_for_one ) {
                    DB::rollBack();
                    abort(422,'Price is bigger than start price!');
                }
                $product->update($keyedProducts[$product->purchase_subject_products_id]);
                $LotAmountRebidding = $LotAmountRebidding + $keyedProducts[$product->purchase_subject_products_id]['price_for_one_rebidding'];
            }
        }
        DB::commit();

        $lot_amounts_rebidding = (array) $application->subject->lot_amounts_rebidding;
        $lot_amounts_rebidding[$lot] = $LotAmountRebidding;
        $application->subject->update(['lot_amounts_rebidding' => $lot_amounts_rebidding, 'price_after_rebidding' => array_sum($lot_amounts_rebidding) ]);

        return ParticipationApplication::with([
            'subject.products',
        ])->find($application->id);
    }

    /**
     * @param $data
     * @param $applicationId
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function storeAuctionLot($data, $applicationId)
    {
        $application = $this->participationApplicationModel->with(['subject.products.purchaseProduct'])->findOrFail($applicationId);

        $this->access('storeAuctionLot',[$application,$data['lot']]);

        $inputAuction = [
            'procedure_id' => $application->procedure_id,
            'procedure_participation_applications_id' => $application->id,
            'inn' => $application->inn,
            'lot' => $data['lot'],
            'amount' => $data['amount']
        ];
        $created = $this->auctionModel->create($inputAuction);
        ParticipationApplicationAuctionLots::dispatchIf($created, [$inputAuction],$application->procedure_id,$data['lot'])->onQueue('server_push');
        $this->processAutobot($application->procedure_id,$data['lot']);

        return $this->listAuctionLot($application->procedure_id,$data['lot']);
    }

    /**
     * @param int $procedureId
     * @param int $lotNumber
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function listAuctionLot(int $procedureId, int $lotNumber)
    {
        $procedure = $this->procedureModel->findOrFail($procedureId);

        $this->access('listAuctionLot',[$this->participationApplicationModel,$procedure]);

        $offers = $this->auctionModel->newQuery()
            ->where('procedure_id','=',$procedureId)
            ->where('lot','=', $lotNumber)
            ->orderBy('amount')
            ->get();

        return $offers;
    }

    /**
     * @param int $procedureId
     * @param int $lot
     * @return array
     */
    public function getBestLotOffer(int $procedureId, int $lot)
    {
        $bestLotsDB = DB::select("select ppa.id as application_id, ppa.procedure_id, ppa.inn, sum(pasp.amount_per_position) as lot_price
        from ".(new Procedure\ParticipationApplication\Subject\Product())->getTable()." pasp
        join ".(new Procedure\ParticipationApplication\Subject())->getTable()." pas on pasp.participation_application_subjects_id = pas.id
        join ".(new Procedure\ParticipationApplication())->getTable()." ppa on pas.procedure_participation_applications_id = ppa.id
        where pasp.deleted_at is null and pasp.lot = ".$lot." and ppa.status not in ('canceled','rejected','draft') and ppa.procedure_id = ".$procedureId."
        group by application_id, ppa.procedure_id, ppa.inn
        order by lot_price");


        return (isset($bestLotsDB[0])) ? $bestLotsDB[0] : false ;
    }

    /**
     * @param int $procedureId
     * @return bool
     */
    public function startAuctionWithBestOffers(int $procedureId, int $lot)
    {
        $bestLot = $this->getBestLotOffer($procedureId,$lot);
        if (!empty($bestLot)) {
            $inputAuction = [
                'procedure_id' => $procedureId,
                'procedure_participation_applications_id' => $bestLot->application_id,
                'inn' => $bestLot->inn,
                'lot' => $lot,
                'amount' => $bestLot->lot_price
            ];
            $this->auctionModel->create($inputAuction);
        }
        return true;
    }

    /**
     * @param int $applicationId
     * @param int $lotNumber
     * @param $data
     * @return mixed
     */
    public function addAutoBot(int $applicationId, int $lotNumber, $data)
    {
        $application = $this->participationApplicationModel->findOrFail($applicationId);
        $this->access('storeAuctionLot',[$application, $lotNumber]);

        $autobot = $application->autobot()->firstOrCreate(['lot' => $lotNumber]);
        $autobot->step = $data['step'];
        $autobot->minimal_price = $data['minimal_price'];
        $autobot->save();

        $this->processAutobot($application->procedure_id,$lotNumber);

        return $autobot;
    }

    /**
     * @param int $applicationId
     * @param int $lotNumber
     * @return bool
     */
    public function deleteAutoBot(int $applicationId, int $lotNumber)
    {
        $application = $this->participationApplicationModel->findOrFail($applicationId);

        $autobot = $application->autobot()->where(['lot' => $lotNumber])->first();
        $autobot->delete();

        return true;
    }

    /**
     * @param int $procedureId
     * @param int $lotNumber
     */
    public function processAutobot(int $procedureId, int $lotNumber) {
        $bestOffer = $this->auctionModel->newQuery()
            ->where('procedure_id',$procedureId)
            ->where('lot',$lotNumber)
            ->orderBy('amount','asc')
            ->first(['inn','amount']);
        if (!$bestOffer) return;

        $matchingAutoBotSettings = $this->autoBotModel->newQuery()
            ->whereHas('participationApplication',function($q) use ($procedureId,$bestOffer){
                $q->where('procedure_id', '=', $procedureId)->where('inn','!=',$bestOffer->inn);
            })
            ->where('lot','=',$lotNumber)
            ->where('minimal_price','<',$bestOffer->amount)
            ->with('participationApplication')
            ->get();

        if (count($matchingAutoBotSettings) > 0) {
            $toKafka = [];
            foreach ($matchingAutoBotSettings AS $thisAutoBotSettings) {
                $amount  = ( ($bestOffer->amount-$thisAutoBotSettings->step) >= $thisAutoBotSettings->minimal_price ) ? $bestOffer->amount-$thisAutoBotSettings->step : $thisAutoBotSettings->minimal_price;
                $inputAuction = [
                    'procedure_id' => $procedureId,
                    'procedure_participation_applications_id' => $thisAutoBotSettings->procedure_participation_applications_id,
                    'inn' => $thisAutoBotSettings->participationApplication->inn,
                    'lot' => $lotNumber,
                    'amount' => $amount
                ];
                $this->auctionModel->create($inputAuction);
                $toKafka[] = $inputAuction;
            }
            ParticipationApplicationAuctionLots::dispatchIf(true, $toKafka,$procedureId,$lotNumber)->onQueue('server_push');
            $this->processAutobot($procedureId,$lotNumber);
        }
    }
}
